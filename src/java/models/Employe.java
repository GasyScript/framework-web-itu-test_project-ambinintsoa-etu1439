package models;

import customAnnotations.MethodUrl;
import generic.controller.ModelView;
import java.sql.Date;
import java.util.HashMap;

/**
 *
 * @author Ambinintsoa
 */
public class Employe {
    private String values;
    private Date dateBla;

    public Date getDateBla() {
        return dateBla;
    }

    public void setDateBla(Date dateBla) {
        this.dateBla = dateBla;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }
    
    @MethodUrl(url="lis")
    public ModelView fetchEmp() {
        ModelView mv = new ModelView();
        
        mv.setUrl("test.jsp");
        
        String[] testdata = {"Karakory","Hello"};
        
        HashMap<String, Object> tdata = new HashMap<>();
        tdata.put("testdata", testdata);
        
        mv.setData(tdata);
        
        return mv;
    }
    
    public void Bla() {
        System.out.println("blablabla");
    }
    
    @MethodUrl(url="saveo")
    public ModelView save() {
        ModelView mv = new ModelView();
        
        mv.setUrl("test.jsp");
        
        System.out.println("saveee oh");
        System.out.println("values: "+getValues());
        System.out.println("date bla: "+getDateBla());
        
        return mv;
    }
}
